package link

import (
    "io"
    "sync"
    "context"
    "fmt"
    "log"
    "sync/atomic"
    "errors"
)

const (
    maxBucketSize = 1 << 18
)

type writeRequest struct {
    packet  *Packet
    written chan struct{} // if written, close this chan
}

type Manager struct {
    conn io.ReadWriteCloser

    links     map[uint32]*Link
    linksLock sync.Mutex

    maxID int32

    bucket      int32         // read bucket, only manager readLoop and link.Read will modify it.
    bucketEvent chan struct{} // every time recv PSH and bucket is bigger than 0, will notify, link.Read will modify.

    ctx           context.Context    // readCtx.Done() can recv means manager is closed
    ctxCancelFunc context.CancelFunc // close the manager
    ctxLock       sync.Mutex         // ensure manager close one time

    writes chan writeRequest

    acceptQueue chan *Link
}

func NewManager(conn io.ReadWriteCloser) *Manager {
    ctx, cancelFunc := context.WithCancel(context.Background())

    manager := &Manager{
        conn: conn,

        links: make(map[uint32]*Link),

        maxID: -1,

        bucket:      maxBucketSize,
        bucketEvent: make(chan struct{}, 1),

        ctx:           ctx,
        ctxCancelFunc: cancelFunc,

        writes: make(chan writeRequest, 1000),

        acceptQueue: make(chan *Link, 1000),
    }

    manager.bucketNotify()

    go manager.readLoop()
    go manager.writeLoop()
    return manager
}

func (m *Manager) bucketNotify() {
    select {
    case m.bucketEvent <- struct{}{}:
    default:
    }
}

func (m *Manager) readPacket() (*Packet, error) {
    header := make(PacketHeader, HeaderLength)
    if _, err := io.ReadFull(m.conn, header); err != nil {
        return nil, fmt.Errorf("manager read packet header: %s", err)
    }

    var payload []byte

    if length := header.PayloadLength(); length != 0 {
        payload = make([]byte, length)
        if _, err := io.ReadFull(m.conn, payload); err != nil {
            return nil, fmt.Errorf("manager read packet payload: %s", err)
        }
    }

    packet, err := Decode(append(header, payload...))
    if err != nil {
        return nil, fmt.Errorf("manager read packet decode: %s", err)
    }

    return packet, nil
}

func (m *Manager) writePacket(p *Packet) error {
    req := writeRequest{
        packet:  p,
        written: make(chan struct{}),
    }

    select {
    case <-m.ctx.Done():
        return io.ErrClosedPipe
    case m.writes <- req:
    }

    select {
    case <-m.ctx.Done():
        return io.ErrClosedPipe
    case <-req.written:
        return nil
    }
}

func (m *Manager) Close() error {
    m.ctxLock.Lock()

    select {
    case <-m.ctx.Done():
        m.ctxLock.Unlock()
        return nil
    default:
        m.ctxCancelFunc()
        m.ctxLock.Unlock()

        m.linksLock.Lock()
        for _, link := range m.links {
            link.managerClosed()
        }
        m.linksLock.Unlock()

        return m.conn.Close()
    }
}

func (m *Manager) returnToken(n int) {
    if atomic.AddInt32(&m.bucket, int32(n)) > 0 {
        m.bucketNotify()
    }
}

// recv FIN and send FIN will remove link
func (m *Manager) removeLink(id uint32) {
    delete(m.links, id)
}

func (m *Manager) readLoop() {
    for {
        select {
        case <-m.ctx.Done():
            return
        case <-m.bucketEvent:
        }

        packet, err := m.readPacket()
        if err != nil {
            log.Println(err)
            m.Close()
            return
        }

        switch {
        case packet.PSH:
            m.linksLock.Lock()
            if link, ok := m.links[packet.ID]; ok {
                link.pushBytes(packet.Payload)
            } else {
                link := newLink(packet.ID, m)
                m.links[link.ID] = link
                link.pushBytes(packet.Payload)
                m.acceptQueue <- link
            }
            m.linksLock.Unlock()
            if atomic.AddInt32(&m.bucket, -int32(packet.Length)) > 0 {
                m.bucketNotify()
            }

        case packet.FIN:
            m.linksLock.Lock()
            if link, ok := m.links[packet.ID]; ok {
                link.close()
            }
            m.linksLock.Unlock()
        }
    }
}

func (m *Manager) writeLoop() {
    for {
        select {
        case <-m.ctx.Done():
            return
        case req := <-m.writes:
            _, err := m.conn.Write(req.packet.Bytes())
            if err != nil {
                log.Println("manager writeLoop:", err)
                m.Close()
                return
            }
            close(req.written)
        }
    }
}

func (m *Manager) NewLink() (*Link, error) {
    m.maxID++
    link := newLink(uint32(m.maxID), m)

    select {
    case <-m.ctx.Done():
        return nil, errors.New("broken manager")
    default:
        m.linksLock.Lock()
        m.links[link.ID] = link
        m.linksLock.Unlock()
        return link, nil
    }
}

func (m *Manager) Accept() (*Link, error) {
    select {
    case <-m.ctx.Done():
        return nil, errors.New("broken manager")
    case link := <-m.acceptQueue:
        return link, nil
    }
}
